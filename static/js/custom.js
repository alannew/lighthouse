import moment from 'moment'
export function indexMethod(index) {
    index++;
    console.log(index)
    return index >= 10 ? index : '0' + index
}

/**
 * 根据当前日期获取本月周信息
 * @param date 当前日期
 * @returns {[]}
 */
export function weeksInfoOfMonth(date) {
    var startYear = moment(date).startOf('month').weekYear();
    var startWeek = moment(date).startOf('month').week();
    var endYear = moment(date).endOf('month').weekYear();
    var endWeek = moment(date).endOf('month').week();
    var output = [];
    if (startYear === endYear) {
        for (var i = startWeek; i <= endWeek; i++) {
            output.push({
                year: startYear,
                week: i,
                startDateStr: moment().year(startYear).week(i).startOf('week').format('MM月DD日'),
                endDateStr: moment().year(startYear).week(i).endOf('week').format('MM月DD日'),
                startDate: moment().year(startYear).week(i).startOf('week').startOf('day'),
                endDate: moment().year(startYear).week(i).endOf('week').endOf('day')
            });
        }
    } else {
        var lastWeekOfYear = moment(date).endOf('month').startOf('week').add('-1', 'days').week();
        for (var j = startWeek; j <= lastWeekOfYear; j++) {
            output.push({
                year: startYear,
                week: j,
                startDateStr: moment().year(startYear).week(j).startOf('week').format('MM月DD日'),
                endDateStr: moment().year(startYear).week(j).endOf('week').format('MM月DD日'),
                startDate: moment().year(startYear).week(j).startOf('week').startOf('day'),
                endDate: moment().year(startYear).week(j).endOf('week').endOf('day')
            });
        }
        output.push({
            year: endYear,
            week: endWeek,
            startDateStr: moment().year(endYear).week(endWeek).startOf('week').format('MM月DD日'),
            endDateStr: moment().year(endYear).week(endWeek).endOf('week').format('MM月DD日'),
            startDate: moment().year(endYear).week(endWeek).startOf('week').startOf('day'),
            endDate: moment().year(endYear).week(endWeek).endOf('week').endOf('day')
        })
    }
    return output;
}

/**
 * x轴数据
 * @param flag
 * @param date
 */
export function xAxisInfo(flag, date) {
    switch (flag) {
        case "week":
            var weeks = [];
            for (var k = 0; k < 7; k++) {
                weeks.push(moment(date).startOf('week').add(k, 'd').format('MM月DD日'))
            }
            return weeks;
        case "month":
            var yearNum = moment(date).year()
            var monthNum = moment(date).month() + 1
            var days;
            switch (monthNum) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    days = 31;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    days = 30;
                    break;
                default:
                    if((yearNum % 4 === 0 && yearNum % 100 !== 0) || yearNum % 400 === 0){
                        days = 29;
                    }else{
                        days = 28;
                    }
            }
            var months = [];
            for (var i = 1; i <= days; i++) {
                months.push(i + '日')
            }
            return months;
        case "year":
            var years = [];
            for (var j = 1; j <= 12; j++) {
                years.push(j + '月')
            }
            return years;
        default:
            return [];
    }
}

