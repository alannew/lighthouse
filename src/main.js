import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
// import locale from 'element-plus/lib/locale/lang/zh-cn'

import axios from 'axios';
import VueAxios from 'vue-axios';
import moment from 'moment'

const app = createApp(App);

app.use(VueAxios, axios);
app.use(store);
app.use(router);
app.use(moment);
app.use(ElementPlus);

app.mount('#app')

