import { createStore } from 'vuex'

export default createStore({
  state: {
    keycloak:{},
    token:'',
    groupList:[],
    deptList:[]
  },
  mutations: {
    setToken(state,param){
      state.token = param;
    }
  },
  actions: {
  },
  modules: {
  }
})
