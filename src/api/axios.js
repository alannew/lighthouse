import axios from "axios";
import { ElMessage } from 'element-plus';
// import { useRouter } from "vue-router";
// import store from '../store/index.js';


// const router = useRouter();

const getLocalStorage = (key) => {
    /*
     * get 获取方法
     * @ param {String}     key 键
     * @ param {String}     expired 存储时为非必须字段，所以有可能取不到，默认为 Date.now+1
     */
    const source = window.localStorage,
        expired = source[`${key}__expires__`] || Date.now + 1;
    const now = Date.now();

    if (now >= expired) {
        delete source[key];
        delete source[`${key}__expires__`];
        return;
    }
    const value = source[key] ? JSON.parse(source[key]) : source[key];
    return value;
}

//post请求头
// axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";
//设置超时
axios.defaults.timeout = 20000;
// axios.defaults.withCredentials = true;


axios.interceptors.request.use(
    config => {
        // if(store.state.token!=null){
        //     config.headers.Authorization = `Bearer `+ store.state.token;
        // }
        // if (getLocalStorage('token') != null) {
        //     config.headers.Authorization = `Bearer ` + getLocalStorage('token');
        //     // config.headers.Authorization = `Bearer ` +"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJzNzBqQ0V1c3pHV1VWZ0hJQndlMXJtX1R1c0lKbXNra3FDTXVKMnZ5MlRRIn0.eyJleHAiOjE2MzM4NTI0MzksImlhdCI6MTYzMzg0ODgzOSwiYXV0aF90aW1lIjoxNjMzODQ4ODM4LCJqdGkiOiJmMmVkNjdkZi02N2EyLTRkNjctYmZhNS1hZjBkNmNjMDNjZjEiLCJpc3MiOiJodHRwczovL2Rldi1tY3AtaWFtLmNucy5jb20uY24vYXV0aC9yZWFsbXMvY25zIiwiYXVkIjpbImNzcGJtIiwiYWNjb3VudCJdLCJzdWIiOiI0M2E4NzI3NC1hOThiLTQzNDQtYjExYi1mNjkyZGRmNGE4YzUiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJjbnNzeXNtYW5hZ2UiLCJub25jZSI6ImNiNmMxYmVlLTkzNWMtNDUyYS04NjVhLWQ2ZThkZTRlZWM0OSIsInNlc3Npb25fc3RhdGUiOiI1ZWQ0MmQ0Zi02YWQwLTQ5ZTgtOTMxMC1hM2FiMTA5Yjk1NjEiLCJhY3IiOiIwIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImNzcGJtIiwicmVzb3VyY2UtY25zZ2F0ZV9yZXNvdXJjZXJvbGUiLCJDTlNfU1lTX01BTkFHRSIsIkFDVF9NQU5BR0VfUk9MRSIsInVtYV9hdXRob3JpemF0aW9uIiwiU1lTX01BTkFHRSJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImNzcGJtIjp7InJvbGVzIjpbInJlc291cmNlLWNuc2dhdGVfcm9sZSJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgZW1haWwgcHJvZmlsZSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6Imd1eGlhb2Rvbmcg6LC35pmT5LicIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiZ3V4aWFvZG9uZyIsImdpdmVuX25hbWUiOiJndXhpYW9kb25nIiwiZmFtaWx5X25hbWUiOiLosLfmmZPkuJwiLCJlbWFpbCI6Imd1eGlhb2RvbmdAY2hpbmFuZXdzLmNvbS5jbiJ9.S_8deq1iU7tPwjarIbEazqSh0zgVhlO29P4HZ-xQZicCfvrFUVnzfMKR4X1oXp0gt-cLNZl8YjoiXf_d76rhZ6qcnHZM8xS3BdRDCuT8N449Ev6_TZA1-iNYcd-NwQxqNpDabEOSRnVqWkIhhoLzbtCUOaejN-wXmp0XF36UY9YwSsYjEAffV8_p-BgRczhDlcT2D117yLlf-fCsB4gTQFRPt6nvKO8CMDC9Nvwkilxyc19Iqpi42wEBYqFkngEW1lq4tBG6fkTJZvi4Xm8yPinX02oC1IdKLrTtDO3v27n5SaPBURRHYdJ3l8bMCw_6l-10A8mZzhWaura9OevUEw";
        // }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    response => {
    // debugger
        if (response.status == 200) {
            return Promise.resolve(response);
        } else {
            return Promise.reject(response);
        }
    },
    error => {
        if (error.response) {
            switch (error.response.status) {
                case 403:
                case 401:
                    window.localStorage.clear();
                    location.href = process.env.VUE_APP_URL; //'https://dev-dsp.cns.com.cn/';
                    break;
            }
        } else {
            console.log(error);
            ElMessage.success({
                title: "提示",
                message: "网络请求失败，请刷新重试"
            });
        }
    }
);
export default {
    post(url, data) {
        return new Promise((resolve, reject) => {
            axios({
                    method: 'post',
                    url,
                    data
                })
                .then(res => {
                    if (res.data.code == 401 || res.data.code == 403) {
                        location.href = process.env.VUE_APP_URL; //'https://dev-dsp.cns.com.cn/';
                    }
                    resolve(res.data)
                })
                .catch(err => {
                    reject(err)
                });
        })
    },

    get(url, data) {
        return new Promise((resolve, reject) => {
            axios({
                    method: 'get',
                    url,
                    params: data,
                })
                .then(res => {
                    if (res.data.code == 401 || res.data.code == 403) {
                        location.href = process.env.VUE_APP_URL; //'https://dev-dsp.cns.com.cn/';
                    }
                    resolve(res.data)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },

    getToken() {
        return `Bearer ` + getLocalStorage('token');
    },
};