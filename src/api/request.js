import http from './axios'

const post = (url, data = {}) =>
    http.post(
        url,
        data
    )
const get = (url, params = {}) =>
    http.get(
        url,
        params
    )

// const prefix = 'https://dev-vs.cns.com.cn/api';
const prefix = '/api';
const prefixTest = 'https://devapi.qweather.com/v7';

//应用管理
//列表
const getAppList = (data) => post(`${prefix}/application/list`, data);
const getAppAllList = () => get(`${prefix}/application/allList`);
//新增app
const addApp = data => post(`${prefix}/application/save`, data);
//app修改
const updateApp = data => post(`${prefix}/application/update`, data);
//删除
const getSecret = (data) => get(`${prefix}/application/secret/${data}`);
const getBaseInfo = () => get(`${prefixTest}/weather/3d?location=101010100&key=5a5b1593f1d04ffc88e1984865f948cd`);

//接口导出
export {
    get,
    post,
    addApp,
    getAppList,
    getAppAllList,
    updateApp,
    getSecret,
    getBaseInfo
}
