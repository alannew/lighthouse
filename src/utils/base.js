export default {
    setLocalStorage(key, value, expired) {
        /*
        * set 存储方法
        * @ param {String}     key 键
        * @ param {String}     value 值，
        * @ param {String}     expired 过期时间，以分钟为单位，非必须
        */
        let source = window.localStorage;
        source[key] = JSON.stringify(value);
        if (expired){
            source[`${key}__expires__`] = Date.now() + 1000*60*expired
        }
        return value;
    },
    getLocalStorage(key) {
        /*
        * get 获取方法
        * @ param {String}     key 键
        * @ param {String}     expired 存储时为非必须字段，所以有可能取不到，默认为 Date.now+1
        */
        const source = window.localStorage,
            expired = source[`${key}__expires__`]||Date.now+1;
        const now = Date.now();

        if ( now >= expired ) {
            delete source[key];
            delete source[`${key}__expires__`];
            return;
        }
        const value = source[key] ? JSON.parse(source[key]) : source[key];
        return value;
    },
    removeLocalStorage(key) {
        const data = window.localStorage,
            value = data[key];
        delete data[key];
        delete data[`${key}__expires__`];
        return value;
    }
}