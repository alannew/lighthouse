import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'

const routes = [
    {
        path: '/',
        name: 'About',
        component: About,
    },
    {
        path: '/info',
        name: 'Home',
        component: Home,
        children: [
            {
                path: "/zbjj",
                component: () =>
                    import ('../views/console/Zbjj.vue')
            },
            {
                path: "/cpxt",
                component: () =>
                    import ('../views/console/Cpxt.vue')
            },
            {
                path: '/bgcx',
                component: () =>
                    import ( '../views/console/Bgcx.vue'),
            },
            {
                path: '/czsc',
                component: () =>
                    import ( '../views/console/Czsc.vue'),
            },
        ]
    },

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router